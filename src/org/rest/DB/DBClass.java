package org.rest.DB;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/** This class returns the Oracle database connect object from a CentOS Oracle
 * Express Virtual Machine
 *
 * The method and variable in this class are static to save resources You only
 * need one instance of this class running. */
public class DBClass
{
	private static DataSource mySQLDataSource = null; //hold the database object
	private static Context context = null; //used to lookup the database connection in weblogic

	/** This is a public method that will return the database connection.
	 * 
	 * @return Database object
	 * @throws Exception */
	private static DataSource getMySQLConnection() throws Exception
	{
		/** check to see if the database object is already defined... if it is,
		 * then return the connection, no need to look it up again. */
		if (mySQLDataSource != null)
		{
			return mySQLDataSource;
		}
		try
		{
			if (context == null)
			{
				context = new InitialContext();
			}
			mySQLDataSource = (DataSource) context.lookup("jdbc/__maResource");//JNDI NAME HERE //jdbc/__Resource_TD5
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return mySQLDataSource;
	}

	/** This method will return the connection to the DB
	 * 
	 * @return Connection to MySQL database. */
	public static Connection returnConnection()
	{
		Connection conn = null;
		try
		{
			conn = getMySQLConnection().getConnection();
			return conn;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return conn;
	}
}