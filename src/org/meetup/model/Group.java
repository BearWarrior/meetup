package org.meetup.model;

import java.util.ArrayList;

/**
 * A class to manage a group in the web application
 * @author Ludovic Clarissou, Thibaut Gallais and Arnaud Ledru
 *
 */
public class Group
{
	private String name;
	private String description;
	private User admin;
	private ArrayList<User> members;
	private String discution;

	/**
	 * contructor with all paramters
	 * @param name
	 * @param description
	 * @param admin
	 * @param members
	 * @param discution
	 */
	public Group(String name, String description, User admin, ArrayList<User> members, String discution)
	{
		super();
		this.name = name;
		this.description = description;
		this.admin = admin;
		this.members = members;
		this.discution = discution;
	}
	/**
	 * a contructor using only the name of the group and email of the owner
	 * @param name
	 * @param emailAdmin
	 */
	public Group(String name, String emailAdmin)
	{
		this.name = name;
		this.admin = new User(emailAdmin);
	}

	/**
	 * a contructor using a name, a descriptio and the email of the admin
	 * @param name
	 * @param description
	 * @param emailAdmin
	 */
	public Group(String name, String description, String emailAdmin)
	{
		super();
		this.name = name;
		this.description = description;
		this.admin = new User(emailAdmin);
	}

	/**
	 * Constructor using only the name
	 * @param name
	 */
	public Group(String name)
	{
		super();
		this.name = name;
		// TODO Link with Database;
	}

	/**
	 * getter for the name
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * setter of the name
	 * @param name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * getter for the name
	 * @return a string with a name of the group
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * setter of the description
	 * @param description
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * getter of the admin 
	 * @return a user
	 */
	public User getAdmin()
	{
		return admin;
	}

	/** 
	 * setter of the admin
	 * @param admin
	 */
	public void setAdmin(User admin)
	{
		this.admin = admin;
	}

	/** 
	 * getter of the members
	 * @return a arraylist with members in a random order
	 */
	public ArrayList<User> getMembers()
	{
		return members;
	}

	/**
	 * setter of the members
	 * @param members
	 */
	public void setMembers(ArrayList<User> members)
	{
		this.members = members;
	}

	/**
	 * getter of the discution
	 * @return
	 */
	public String getDiscution()
	{
		return discution;
	}

	/**
	 * setter of the discution
	 * @param discution
	 */
	public void setDiscution(String discution)
	{
		this.discution = discution;
	}
}