
package org.meetup.model;

import java.util.ArrayList;

/**
 * A simple class, to manage a user of the website Meetup
 * @author Ludovic Clarissou, Thibaut Gallais and Arnaud Ledru
 *
 */
public class User
{
	private String name;
	private String surname;
	private String email;
	private String biographie;

	private ArrayList<Group> group_joined;


	/**
	 * A other contrustor to initialise fields
	 * @param name
	 * @param surname
	 * @param email
	 * @param biographie
	 */
	public User(String name, String surname, String email, String biographie)
	{
		super();
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.biographie = biographie;
		//TODO wtf
		this.group_joined = group_joined;
	}

	/**
	 * A constructor using only a email address
	 * @param email
	 */
	public User(String email)
	{
		//TODO Create User with the database
		this.email = email;
	}
	
	/** 
	 * A constructor with the name, surname and email adress
	 * @param name
	 * @param surname
	 * @param email
	 */
	public User(String name, String surname, String email)
	{
		super();
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.biographie = "";
	}


	/**
	 * Getter for the name
	 * @return string with the name
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Setter for the name
	 * @param name
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	
	/**
	 * getter for the surname
	 * @return a string with the surname
	 */
	public String getSurname()
	{
		return surname;
	}
	/**
	 * setter for the surname
	 * @param surname
	 */
	public void setSurname(String surname)
	{
		this.surname = surname;
	}
	/**
	 * getter for the email
	 * @return
	 */
	public String getEmail()
	{
		return email;
	}
	/**
	 * setter for the email
	 * @param email
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}
	/**
	 * getter for the bio
	 * @return
	 */
	public String getBiographie()
	{
		return biographie;
	}
	 /**
	  * setter for the bio
	  * @param biographie
	  */
	public void setBiographie(String biographie)
	{
		this.biographie = biographie;
	}

	/** 
	 * Get the list of group a user joined
	 * @return an arrayList with the group in a random order.
	 */
	public ArrayList<Group> getGroup_joined()
	{
		return group_joined;
	}

	public void setGroup_joined(ArrayList<Group> group_joined)
	{
		this.group_joined = group_joined;
	}
}
