package org.meetup.RESTfulService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.ws.rs.core.Response;

import org.meetup.model.Group;
import org.meetup.model.User;
import org.rest.DB.DBClass;
import org.rest.util.ToJSON;

/** A class who link the API and the BDD
 * 
 * @author Ludovic Clarissou, Thibaut Gallais and Arnaud Ledru */
public class BDDManager
{
	/** A method that insert a new user in the DB
	 * 
	 * @param user the user to add
	 * @return Response.ok if OK, Response.serverError otherwise */
	public static Response addNewUser(User user)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			PreparedStatement insert = connection
					.prepareStatement("INSERT INTO user (mail, name, surname) " + " VALUES (?,?,?);");
			insert.setString(1, user.getEmail());
			insert.setString(2, user.getName());
			insert.setString(3, user.getSurname());
			int httpCode = insert.executeUpdate();
			connection.close();
			if (httpCode == 1)
				return Response.ok().build();
			else
				return Response.serverError().build();
		}
		catch (SQLException e)
		{
			return Response.serverError().build();
		}
	}

	/** Select a user from the database, only the email is needed
	 * 
	 * @param user the user to get
	 * @return all the information about the user in JSON or
	 *         Response.status(400) with "error" otherwise */
	public static Response getUser(User user)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			ResultSet result = stmt.executeQuery("SELECT * FROM user WHERE mail = \"" + user.getEmail() + "\";");
			connection.close();
			if (result.isBeforeFirst())
			{
				ToJSON tojson = new ToJSON();
				return Response.status(200).entity(tojson.toJSONArray(result).toString()).build();
			}
			else
			{
				return Response.status(400).entity("error").build();
			}
		}
		catch (Exception e)
		{
			return Response.status(400).entity("error").build();
		}
	}

	/** Allow a user to change its name, surname and/or biography
	 * 
	 * @param user the user to change name, surmane and/or biography
	 * @return Response.ok() if OK, Response.serverError() otherwise */
	public static Response changeUserAttributes(User user)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			//NAME
			PreparedStatement insert = connection
					.prepareStatement("UPDATE user SET name = " + "?" + " WHERE mail = " + "?;");
			insert.setString(1, user.getName());
			insert.setString(2, user.getEmail());
			insert.executeUpdate();
			//SURNAME
			insert = connection.prepareStatement("UPDATE user SET surname = " + "?" + " WHERE mail = " + "?;");
			insert.setString(1, user.getSurname());
			insert.setString(2, user.getEmail());
			insert.executeUpdate();
			//BIO
			insert = connection.prepareStatement("UPDATE user SET biographie = " + "?" + " WHERE mail = " + "?;");
			insert.setString(1, user.getBiographie());
			insert.setString(2, user.getEmail());
			insert.executeUpdate();
			connection.close();
			return Response.ok().build();
		}
		catch (Exception e)
		{
			return Response.serverError().build();
		}
	}

	/** delete a user account based on its mail and transfer all its message
	 * under the "deletedUser" account
	 * 
	 * @param user the user to delete
	 * @return Response.ok() if OK, Response.serverError() otherwise */
	public static Response deleteAccount(User user)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			//Change messages owner
			Statement stmt = connection.createStatement();
			int httpCode2 = stmt.executeUpdate(
					"UPDATE message SET mailUser = \"deletedUser\" WHERE mailUser = \"" + user.getEmail() + "\";");
			//Delete the user
			int httpCode = stmt.executeUpdate("DELETE FROM user WHERE mail = \"" + user.getEmail() + "\";");
			connection.close();
			if (httpCode == 1)
				return Response.ok().build();
			else
				return Response.serverError().build();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	/** Add a new group in the database and add the creator to this group
	 * 
	 * @param group the group with the name and the mail of the admin to create
	 * @return Response.ok() if OK, Response.serverError() otherwise */
	public static Response createGroup(Group group)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			PreparedStatement insert = connection
					.prepareStatement("INSERT INTO `group` (nom, description, mailAdmin) " + " VALUES (?,?,?);");
			insert.setString(1, group.getName());
			insert.setString(2, group.getDescription());
			insert.setString(3, group.getAdmin().getEmail());
			int httpCode = insert.executeUpdate();

			Statement stmt = connection.createStatement();
			httpCode = stmt.executeUpdate("INSERT INTO `user_group` (mail, nomGroup) VALUES (\""
					+ group.getAdmin().getEmail() + "\",\"" + group.getName() + "\");");

			connection.close();
			if (httpCode == 1)
				return Response.ok().build();
			else
				return Response.serverError().build();
		}
		catch (Exception e)
		{
			return Response.serverError().build();
		}
	}

	/** Change the description of a group based on its name
	 * 
	 * @param group the group to change description
	 * @return Response.ok() if OK, Response.serverError() otherwise */
	public static Response changeGroupAttributes(Group group)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			PreparedStatement insert = connection
					.prepareStatement("UPDATE `group` SET description = " + "?" + " WHERE nom = " + "?;");
			insert.setString(1, group.getDescription());
			insert.setString(2, group.getName());
			int httpCode = insert.executeUpdate();
			connection.close();
			if (httpCode == 1)
				return Response.ok().build();
			else
				return Response.serverError().build();
		}
		catch (Exception e)
		{
			return Response.serverError().build();
		}
	}

	/** delete a group from its name
	 * 
	 * @param group the group to delete
	 * @return Response.ok() if OK, Response.serverError() otherwise */
	public static Response deleteGroup(Group group)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			int httpCode = stmt.executeUpdate("DELETE FROM `group` WHERE nom = \"" + group.getName() + "\";");
			connection.close();
			if (httpCode == 1)
				return Response.ok().build();
			else
				return Response.serverError().build();
		}
		catch (Exception e)
		{
			return Response.serverError().build();
		}
	}

	/** returns all the groups where the user is admin
	 * 
	 * @param user the user to get group where he is admin
	 * @return the list of groups where the user is admin in JSON,
	 *         Response.status(400).entity("error") otherwise */
	public static Response getUserGroupsWhereAdmin(User user)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			ResultSet result = stmt.executeQuery(
					"SELECT *, (SELECT count(*) FROM user_group ug WHERE g.nom = ug.nomGroup) nbMembers FROM `group` g WHERE mailAdmin = \""
							+ user.getEmail() + "\";");
			connection.close();
			if (result.isBeforeFirst())
			{
				ToJSON tojson = new ToJSON();
				return Response.status(200).entity(tojson.toJSONArray(result).toString()).build();
			}
			else
			{
				return Response.status(400).entity("error").build();
			}
		}
		catch (Exception e)
		{
			return Response.status(400).entity("error").build();
		}
	}

	/** returns the name of the user
	 * 
	 * @param user
	 * @return the name of the user, Response.status(400) otherwise */
	public static Response getUserName(User user)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			ResultSet result = stmt.executeQuery("SELECT name FROM user WHERE mail = \"" + user.getEmail() + "\";");
			connection.close();
			if (result.isBeforeFirst())
			{
				ToJSON tojson = new ToJSON();
				return Response.status(200).entity(tojson.toJSONArray(result).toString()).build();
			}
			else
			{
				return Response.status(400).entity("no user of this name").build();
			}
		}
		catch (Exception e)
		{
			return Response.status(400).entity("error").build();
		}
	}

	/** returns the surname of the user
	 * 
	 * @param user
	 * @return the surname of the user, Response.status(400))otherwise */
	public static Response getUserSurname(User user)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			ResultSet result = stmt.executeQuery("SELECT surname FROM user WHERE mail = \"" + user.getEmail() + "\";");
			connection.close();
			if (result.isBeforeFirst())
			{
				ToJSON tojson = new ToJSON();
				return Response.status(200).entity(tojson.toJSONArray(result).toString()).build();
			}
			else
			{
				return Response.status(400).entity("no user of this name").build();
			}
		}
		catch (Exception e)
		{
			return Response.status(400).entity("error").build();
		}
	}

	/** returns the biography of the user
	 * 
	 * @param user
	 * @return the biography of the user, Response.status(400) otherwise */
	public static Response getUserBiography(User user)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			ResultSet result = stmt
					.executeQuery("SELECT biographie FROM user WHERE mail = \"" + user.getEmail() + "\";");
			connection.close();
			if (result.isBeforeFirst())
			{
				ToJSON tojson = new ToJSON();
				return Response.status(200).entity(tojson.toJSONArray(result).toString()).build();
			}
			else
			{
				return Response.status(400).entity("no user of this name").build();
			}
		}
		catch (Exception e)
		{
			return Response.status(400).entity("error").build();
		}
	}

	/** returns the description of the group
	 * 
	 * @param group
	 * @return the description of the group, Response.status(400) otherwise */
	public static Response getGroupDescription(Group group)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			ResultSet result = stmt
					.executeQuery("SELECT description FROM `group` WHERE nom = \"" + group.getName() + "\";");
			connection.close();
			if (result.isBeforeFirst())
			{
				ToJSON tojson = new ToJSON();
				return Response.status(200).entity(tojson.toJSONArray(result).toString()).build();
			}
			else
			{
				return Response.status(400).entity("no group of this name").build();
			}
		}
		catch (Exception e)
		{
			return Response.status(400).entity("error").build();
		}
	}

	/** returns the admin of the group
	 * 
	 * @param group
	 * @return the admin of the group, Response.status(400) otherwise */
	public static Response getGroupAdmin(Group group)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			ResultSet result = stmt.executeQuery(
					"SELECT `user`.name,`user`.surname, `user`.biographie FROM `group`,`user` WHERE `group`.mailAdmin = `user`.mail and `group`.nom = \""
							+ group.getName() + "\";");
			connection.close();
			if (result.isBeforeFirst())
			{
				ToJSON tojson = new ToJSON();
				return Response.status(200).entity(tojson.toJSONArray(result).toString()).build();
			}
			else
			{
				return Response.status(400).entity("no group of this name").build();
			}
		}
		catch (Exception e)
		{
			return Response.status(400).entity("error").build();
		}
	}

	/** returns all the members of the group in parameter
	 * 
	 * @param group
	 * @return all the members of the group in parameter, Response.status(400)
	 *         otherwise */

	public static Response getGroupMembers(Group group)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			ResultSet result = stmt.executeQuery(
					"SELECT user.mail, user.name, user.surname, user.biographie FROM user_group, user WHERE user_group.mail = user.mail AND user_group.nomGroup = \""
							+ group.getName() + "\";");
			connection.close();
			if (result.isBeforeFirst())
			{
				ToJSON tojson = new ToJSON();
				return Response.status(200).entity(tojson.toJSONArray(result).toString()).build();
			}
			else
			{
				return Response.status(400).entity("no members or no group of this name").build();
			}
		}
		catch (Exception e)
		{
			return Response.status(400).entity("error").build();
		}
	}

	/** Select a group by its name
	 * 
	 * @param group
	 * @return all th information about this group, Response.status(400)
	 *         otherwise */
	public static Response getGroup(Group group)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			ResultSet result = stmt.executeQuery("SELECT * FROM `group` WHERE nom = \"" + group.getName() + "\";");
			connection.close();
			if (result.isBeforeFirst())
			{
				ToJSON tojson = new ToJSON();
				return Response.status(200).entity(tojson.toJSONArray(result).toString()).build();
			}
			else
			{
				return Response.status(400).entity("no group of this name").build();
			}
		}
		catch (Exception e)
		{
			return Response.status(400).entity("error").build();
		}
	}

	/** Allow a user to join a group
	 * 
	 * @param user only the mail is used
	 * @param group only the name is used
	 * @return Response.ok() if OK, Response.serverError() otherwise */
	public static Response joinGroup(User user, Group group)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			int httpCode = stmt.executeUpdate("INSERT INTO user_group (mail, nomGroup) VALUES (\"" + user.getEmail()
					+ "\", \"" + group.getName() + "\");");
			connection.close();
			if (httpCode == 1)
				return Response.ok().build();
			else
				return Response.serverError().build();
		}
		catch (Exception e)
		{
			return Response.serverError().build();
		}
	}

	/** Allow a user to leave a group
	 * 
	 * @param user only the email is used
	 * @param group only the name is used
	 * @return Response.ok() if OK, Response.serverError() otherwise */
	public static Response leaveGroup(User user, Group group)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			int httpCode = stmt.executeUpdate("DELETE FROM user_group WHERE mail = \"" + user.getEmail()
					+ "\" AND nomGroup = \"" + group.getName() + "\";");
			connection.close();
			if (httpCode == 1)
				return Response.ok().build();
			else
				return Response.serverError().build();
		}
		catch (Exception e)
		{
			return Response.serverError().build();
		}
	}

	/** Allow to write a message in the dashboard of a group
	 * 
	 * @param user
	 * @param group
	 * @param msg
	 * @return Response.ok() if OK, Response.serverError() otherwise */
	public static Response addMessage(User user, Group group, String msg)
	{
		//INSERT INTO `message` (`id_Message`, `id_User`, `Msg`, `time`, `id_Group`) VALUES ('2', 'ludo@gmail.com', 'blblblblbl', '2016-11-19 00:00:00.000000', '1');
		Connection connection = DBClass.returnConnection();
		try
		{
			PreparedStatement insert = connection
					.prepareStatement("INSERT INTO `message` (`mailUser`, `Msg`, `nomGroup`) VALUES " + "(?,?,?);");
			insert.setString(1, user.getEmail());
			insert.setString(2, msg);
			insert.setString(3, group.getName());
			int httpCode = insert.executeUpdate();
			connection.close();
			if (httpCode == 1)
				return Response.ok().build();
			else
				return Response.serverError().build();
		}
		catch (SQLException e)
		{
			return Response.serverError().build();
		}
	}

	/** Get the messages from a group dashboard
	 * 
	 * @param group
	 * @return the message, Response.status(400) otherwise */
	public static Response getMessages(Group group)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			ResultSet result = stmt.executeQuery(
					"SELECT name, surname, mail, Msg FROM message m, `user` u WHERE m.mailUser = u.mail AND nomGroup = \""
							+ group.getName() + "\" ORDER BY time ASC;");
			connection.close();
			if (result.isBeforeFirst())
			{
				ToJSON tojson = new ToJSON();
				return Response.status(200).entity(tojson.toJSONArray(result).toString()).build();
			}
			else
			{
				return Response.status(400).entity("error").build();
			}
		}
		catch (Exception e)
		{
			return Response.serverError().build();
		}
	}

	/** returns list of group where user is not admin but member
	 * 
	 * @param user
	 * @return */
	public static Response getGroupsOfUser(User user)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			ResultSet result = stmt.executeQuery(

					"SELECT *, (SELECT count(*) FROM user_group ug WHERE g.nom = ug.nomGroup) nbMembers  FROM `group` g where g.nom NOT IN ( SELECT nom FROM `group` WHERE mailAdmin = \""
							+ user.getEmail()
							+ "\")  AND g.nom NOT IN ( SELECT nom FROM `group` g where g.nom NOT IN( SELECT nomGroup FROM `user_group` WHERE mail = \""
							+ user.getEmail() + "\"))");

			connection.close();
			if (result.isBeforeFirst())
			{
				ToJSON tojson = new ToJSON();
				return Response.status(200).entity(tojson.toJSONArray(result).toString()).build();
			}
			else
			{
				return Response.status(400).entity("error").build();
			}
		}
		catch (Exception e)
		{
			return Response.serverError().build();
		}
	}

	/** returns list of group where user is neither admin neither member
	 * 
	 * @param user
	 * @return Response.ok() if OK, Response.serverError() otherwise */
	public static Response getGroupsWhereUserNotMember(User user)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			ResultSet result = stmt.executeQuery(

					"SELECT *, (SELECT count(*) FROM user_group ug WHERE g.nom = ug.nomGroup) nbMembers  FROM `group` g where g.nom NOT IN( SELECT nomGroup FROM `user_group` WHERE mail = \""
							+ user.getEmail() + "\");");

			connection.close();

			ToJSON tojson = new ToJSON();
			return Response.status(200).entity(tojson.toJSONArray(result).toString()).build();
		}
		catch (Exception e)
		{
			return Response.serverError().build();
		}
	}

	/** returns list of group where user is neither admin neither member
	 * 
	 * @param user
	 * @return Response.ok() if OK, Response.serverError() otherwise */
	public static Response getMessage(String email, String id)
	{
		Connection connection = DBClass.returnConnection();
		try
		{
			Statement stmt = connection.createStatement();
			ResultSet result = stmt.executeQuery("SELECT * from user_group where mail = \"" + email + "\";");
			if (result.isBeforeFirst()) //User exists
			{
				result = stmt.executeQuery("SELECT Msg from message where id_Message = " + id + ";");
				connection.close();
				if (result.isBeforeFirst()) //message exists
				{
					ToJSON tojson = new ToJSON();
					return Response.status(200).entity(tojson.toJSONArray(result).toString()).build();
				}
				else
				{
					return Response.status(400).entity("error : no Msg").build();
				}
			}
			else
			{
				return Response.status(400).entity("error : access refused").build();
			}
		}
		catch (Exception e)
		{
			return Response.serverError().build();
		}
	}
}
