package org.meetup.RESTfulService;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.meetup.model.Group;
import org.meetup.model.User;

/** MeetUp A class with all the fonctions that can be called trough the restfull
 * API See also the user guide.
 * 
 * @author Ludovic Clarissou, Thibaut Gallais and Arnaud Ledru */
@Path("MeetUp")
public class MeetUp
{
	/** Method to signup from a form in HTML
	 * 
	 * @param name
	 * @param surname
	 * @param email
	 * @return 
	 * */
	@POST
	@Path("signUp")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response signUp(@FormParam("name") String name, @FormParam("surname") String surname,
			@FormParam("email") String email)
	{
		return BDDManager.addNewUser(new User(name, surname, email));
	}
	
	/** Method to signup from queryParam
	 * 
	 * @param name
	 * @param surname
	 * @param email
	 * @return */
	@POST
	@Path("signUp")
	public Response signUp_bis(@QueryParam("name") String name, @QueryParam("surname") String surname,
			@QueryParam("email") String email)
	{
		return BDDManager.addNewUser(new User(name, surname, email));
	}
	

	/** A method to change user attributes (namesn, surname, biographie) based from a form in HTML
	 * on his email
	 * 
	 * @param email
	 * @param name
	 * @param surname
	 * @param biography
	 * @return */
	@PUT
	@Path("changeUserAttributes")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response changeUserAttributes(@FormParam("email") String email, @FormParam("name") String name,
			@FormParam("surname") String surname, @FormParam("bio") String biography)
	{
		return BDDManager.changeUserAttributes(new User(name, surname, email, biography));
	}
	
	/** A method to change the attribute of an user from queryParam
	 * 
	 * @param email
	 * @param name
	 * @param surname
	 * @param biography
	 * @return */
	@PUT
	@Path("changeUserAttributes")
	public Response changeUserAttributes_bis(@QueryParam("email") String email, @QueryParam("name") String name,
			@QueryParam("surname") String surname, @QueryParam("bio") String biography)
	{
		return BDDManager.changeUserAttributes(new User(name, surname, email, biography));
	}
	

	/** Method to delete an account from a form in HTML
	 * 
	 * @param email
	 * @return */
	@POST //POST BECAUSE WE HAVE TO UPDATE THE DATABASE BEFORE DELETING THE USER
	@Path("deleteAccount")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response deleteAccount(@FormParam("email") String email)
	{
		return BDDManager.deleteAccount(new User(email));
	}
	
	/** delete an account from QueryParam
	 * 
	 * @param email
	 * @return */
	@POST  //POST BECAUSE WE HAVE TO UPDATE THE DATABASE BEFORE DELETING THE USER
	@Path("deleteAccount")
	public Response deleteAccount_bis(@QueryParam("email") String email)
	{
		return BDDManager.deleteAccount(new User(email));
	}
	

	/** Method to create a group from a form in html
	 * 
	 * @param email
	 * @param groupName
	 * @return */
	@POST
	@Path("createGroup")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response createGroup(@FormParam("email") String email, @FormParam("groupName") String groupName)
	{

		return BDDManager.createGroup(new Group(groupName, "", email));
	}
	
	/** Method to create a group from queryParam
	 * 
	 * @param email
	 * @param groupName
	 * @return */
	@POST
	@Path("createGroup")
	public Response createGroup_bis(@QueryParam("email") String email, @QueryParam("groupName") String groupName)
	{

		return BDDManager.createGroup(new Group(groupName, "", email));
	}
	

	/** Method to change the description of a group based on its name from a form in HTML
	 * 
	 * @param name
	 * @param description
	 * @return */
	@PUT
	@Path("changeGroupAttributes")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response changeGroupAttributes(@FormParam("name") String name, @FormParam("description") String description)
	{
		return BDDManager.changeGroupAttributes(new Group(name, description, "_"));
	}
	
	/** change description of a group from a queryParam
	 * 
	 * @param name
	 * @param description
	 * @return */
	@PUT
	@Path("changeGroupAttributes")
	public Response changeGroupAttributes_bis(@QueryParam("name") String name, @QueryParam("description") String description)
	{
		return BDDManager.changeGroupAttributes(new Group(name, description, "_"));
	}
	

	/** Method to add a message in a group dashboard from a form in HTML
	 * 
	 * @param email
	 * @param name
	 * @param msg
	 * @return */
	@POST
	@Path("writeMessage")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response addMessage(@FormParam("email") String email, @FormParam("name") String name,
			@FormParam("message") String msg)
	{
		return BDDManager.addMessage(new User(email), new Group(name, email), msg);
	}
	
	/** add a message to a group dashboard from queryParam
	 * 
	 * @param email
	 * @param name
	 * @param msg
	 * @return */
	@POST
	@Path("writeMessage")
	public Response addMessage_bis(@QueryParam("email") String email, @QueryParam("name") String name,
			@QueryParam("message") String msg)
	{
		return BDDManager.addMessage(new User(email), new Group(name, email), msg);
	}
	
	
	/** Delete a group 
	 * 
	 * @param nomGroup
	 * @return */
	@DELETE
	@Path("deleteGroup/{nomGroup}")
	public Response deleteGroup(@PathParam("nomGroup") String nomGroup)
	{
		return BDDManager.deleteGroup(new Group(nomGroup));
	}

	/** A method who returns the name of the user
	 * 
	 * @param email
	 * @return */
	@GET
	@Path("{email}/name")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getName(@PathParam("email") String email)
	{
		return BDDManager.getUserName(new User(email));
	}

	/** A method who returns the name of the user
	 * 
	 * @param email
	 * @return */
	@GET
	@Path("{email}/surname")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNamgetSurname(@PathParam("email") String email)
	{
		return BDDManager.getUserSurname(new User(email));
	}

	/** A method who returns the name of the user
	 * 
	 * @param email
	 * @return */
	@GET
	@Path("{email}/biography")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBiography(@PathParam("email") String email)
	{
		return BDDManager.getUserBiography(new User(email));
	}

	/** A method who returns the members of a group
	 * 
	 * @param name
	 * @return */
	@GET
	@Path("members/{groupName}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMembers(@PathParam("groupName") String name)
	{
		return BDDManager.getGroupMembers(new Group(name));
	}

	/** A method who returns the description of a group
	 * 
	 * @param name
	 * @return */
	@GET
	@Path("group/{name}/description")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDescription(@PathParam("name") String name)
	{
		return BDDManager.getGroupDescription(new Group(name));
	}

	/** A method who returns the admin of a group
	 * 
	 * @param name
	 * @return */
	@GET
	@Path("group/{name}/admin")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAdmin(@PathParam("name") String name)
	{
		System.err.println("In group/cuisine/admin");
		return BDDManager.getGroupAdmin(new Group(name));
	}

	/** Method to join a group
	 * 
	 * @param email
	 * @param name
	 * @return */
	@POST
	@Path("{email}/joinGroup/{groupName}")
	public Response joinGroup(@PathParam("email") String email, @PathParam("groupName") String name)
	{
		return BDDManager.joinGroup(new User(email), new Group(name));
	}

	/** Method which allow you to leave a group
	 * 
	 * @param email
	 * @param name
	 * @return */
	@DELETE
	@Path("{email}/leaveGroup/{groupName}")
	public Response leaveGroup(@PathParam("email") String email, @PathParam("groupName") String name)
	{
		return BDDManager.leaveGroup(new User(email), new Group(name));
	}

	/** Method to get the profile of a user based on the email
	 * 
	 * @param email
	 * @return */
	@GET
	@Path("viewProfile/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewProfile(@PathParam("email") String email)
	{
		return BDDManager.getUser(new User(email));
	}

	/** View a group based on its name
	 * 
	 * @param nameGroup
	 * @return */
	@GET
	@Path("viewGroup/{nameGroup}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewGroup(@PathParam("nameGroup") String nameGroup)
	{
		return BDDManager.getGroup(new Group(nameGroup));
	}

	/** Get the message in the dashboard of a group
	 * 
	 * @param nameGroup
	 * @return */
	@GET
	@Path("getMessages/{nameGroup}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessages(@PathParam("nameGroup") String nameGroup)
	{
		return BDDManager.getMessages(new Group(nameGroup));
	}

	/** Get the list of the group of a user where he is admin based on the user
	 * email
	 * 
	 * @param email
	 * @return */
	@GET
	@Path("{email}/myGroups")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserGroupsWhereAdmin(@PathParam("email") String email)
	{
		return BDDManager.getUserGroupsWhereAdmin(new User(email));
	}

	/** Get the group a user is part of but not admin
	 * 
	 * @param email
	 * @return */
	@GET
	@Path("getGroupsOfUser/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupsOfUser(@PathParam("email") String email)
	{
		return BDDManager.getGroupsOfUser(new User(email));
	}

	/** Get the groups of a user where he is not admin nor member
	 * 
	 * @param email
	 * @return */
	@GET
	@Path("getGroupsWhereUserNotMember/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupsWhereUserNotMember(@PathParam("email") String email)
	{
		return BDDManager.getGroupsWhereUserNotMember(new User(email));
	}

	/** Get the groups of a user where he is not admin nor member
	 * 
	 * @param email
	 * @return */
	@GET
	@Path("message/{email}/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessage(@PathParam("email") String email, @PathParam("id") String id)
	{
		return BDDManager.getMessage(email, id);
	}
}