package org.meetup.RESTfulService;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
/**
 * Empty class to setup the path
 * 
 *
 */
@ApplicationPath("MeetUpApp")
public class MeetUpApplication extends Application
{

}
