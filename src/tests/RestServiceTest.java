package tests;

import static org.junit.Assert.assertEquals;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.apache.commons.dbcp.BasicDataSource;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;
import org.meetup.RESTfulService.MeetUp;


public class RestServiceTest extends JerseyTest
{
	MeetUp meetUp;

	@Override
	public Application configure()
	{
		enable(TestProperties.LOG_TRAFFIC);
		enable(TestProperties.DUMP_ENTITY);

		Properties prop = new Properties();
		prop.put(Context.INITIAL_CONTEXT_FACTORY, "com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
		prop.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");
		prop.setProperty("org.omg.CORBA.ORBInitialPort", "3700");
		
		try {
			BasicDataSource dataSource = new BasicDataSource();
			dataSource.setDriverClassName("com.mysql.jdbc.Driver");
			dataSource.addConnectionProperty("databaseName", "meetup");
			dataSource.addConnectionProperty("connectionAttributes", ";create=true");
			dataSource.addConnectionProperty("ServerName", "localhost");
			dataSource.addConnectionProperty("User", "root");
			dataSource.addConnectionProperty("Password", "root");
			dataSource.addConnectionProperty("PortNumber", "3306");
			dataSource.addConnectionProperty("Url", "jdbc:mysql://localhost:3306/meetup");
			dataSource.addConnectionProperty("URL", "jdbc:mysql://localhost:3306/meetup");
			
			InitialContext initialContext = new InitialContext(prop);
			initialContext.rebind("jdbc/_maResource", dataSource);
		} catch (NamingException e) 
		{
			e.printStackTrace();
		}
		
		ResourceConfig resourceConfig = new ResourceConfig(MeetUp.class);
		return resourceConfig;
	}

	public RestServiceTest()
	{
		meetUp = new MeetUp();
		System.out.println(meetUp);
	}

	@Test
	public void testCreateUser()
	{
		Response output = target("MeetUp/viewProfile/a@a.aa").request().get();
		assertEquals("should return 200", 200, output.getStatus());
	}
}
