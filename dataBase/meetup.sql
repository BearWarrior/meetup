-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 15 Décembre 2016 à 17:52
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `meetup`
--

-- --------------------------------------------------------

--
-- Structure de la table `group`
--

CREATE TABLE `group` (
  `nom` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `mailAdmin` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `group`
--

INSERT INTO `group` (`nom`, `description`, `mailAdmin`) VALUES
('group of ludo', 'this is the group of ludo', 'ludo@gmail.com'),
('groupe1', 'First group ever', 'thgallais@gmail.com'),
('monGroup', 'monGroupmonGroup', 'thgallais@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `id_Message` int(11) NOT NULL,
  `mailUser` varchar(45) NOT NULL,
  `Msg` varchar(255) DEFAULT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `nomGroup` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `message`
--

INSERT INTO `message` (`id_Message`, `mailUser`, `Msg`, `time`, `nomGroup`) VALUES
(75, 'a@a.aa', 'MOUHAHAHAHA I\'m here', '2016-12-15 17:50:50', 'group of ludo'),
(76, 'thgallais@gmail.com', 'Hey, what are you doing here ?', '2016-12-15 17:51:48', 'group of ludo'),
(77, 'thgallais@gmail.com', 'Talk about whatever you want', '2016-12-15 17:52:04', 'monGroup');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `mail` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `surname` varchar(45) DEFAULT NULL,
  `biographie` varchar(255) NOT NULL DEFAULT 'no biography yet'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`mail`, `name`, `surname`, `biographie`) VALUES
('a@a.aa', 'Jean', 'Dujardin', 'Biography of Jean Dujardin'),
('arnaud@gmail.com', 'arnaud', 'ledru', 'no biography yet'),
('deletedUser', 'deletedUser', 'deletedUser', 'deletedUser'),
('email', 'first name changed', 'last name changed', 'My biographie'),
('ludo@gmail.com', 'ludo', 'clari', 'a'),
('thgallais@gmail.com', 'thibautazezaeaze', 'gallais', 'aze');

-- --------------------------------------------------------

--
-- Structure de la table `user_group`
--

CREATE TABLE `user_group` (
  `mail` varchar(45) NOT NULL,
  `nomGroup` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user_group`
--

INSERT INTO `user_group` (`mail`, `nomGroup`) VALUES
('a@a.aa', 'group of ludo'),
('ludo@gmail.com', 'group of ludo'),
('thgallais@gmail.com', 'group of ludo'),
('arnaud@gmail.com', 'groupe1'),
('ludo@gmail.com', 'groupe1'),
('thgallais@gmail.com', 'groupe1'),
('thgallais@gmail.com', 'monGroup');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`nom`),
  ADD KEY `indxMailAdmin` (`mailAdmin`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id_Message`),
  ADD KEY `fk_Message_Group1_idx` (`nomGroup`),
  ADD KEY `fk_Message_User1_idx` (`mailUser`) USING BTREE;

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`mail`);

--
-- Index pour la table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`mail`,`nomGroup`),
  ADD KEY `fk_User_has_User_Group` (`nomGroup`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `id_Message` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `group`
--
ALTER TABLE `group`
  ADD CONSTRAINT `group_ibfk_1` FOREIGN KEY (`mailAdmin`) REFERENCES `user` (`mail`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`mailUser`) REFERENCES `user` (`mail`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_ibfk_NomGroupe` FOREIGN KEY (`nomGroup`) REFERENCES `group` (`nom`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `user_group`
--
ALTER TABLE `user_group`
  ADD CONSTRAINT `fk_User_has_Group_User` FOREIGN KEY (`mail`) REFERENCES `user` (`mail`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_User_has_User_Group` FOREIGN KEY (`nomGroup`) REFERENCES `group` (`nom`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
