$(function() {

	var user = $.cookie('email');
	var group = $.cookie('group');

	if(user === undefined || group == undefined)
	{
		document.location.href = "home.html";
	}
	else
	{
		document.getElementById('nameGroupForm').value = group;
		document.getElementById('emailMessage').value = user;
		document.getElementById('groupMessage').value = group;

		//___________________________________USER INFOS_________________________________________
		$.getJSON("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/viewProfile/" +user,
				function(data) {
			document.getElementById('name_user').innerHTML =data[0].name;
			document.getElementById('lastname_user').innerHTML=data[0].surname;
			document.getElementById('email_user').innerHTML=data[0].mail;
		});


		//___________________________________GROUP INFOS_________________________________________
		$.getJSON("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/viewGroup/" + group,
				function(data) 
				{
			$( "#groupName" ).text(	data[0].nom);

			$.getJSON("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/viewProfile/" + data[0].mailAdmin ,
					function(data) 
					{
				$( "#adminName" ).text(	data[0].name + " " + data[0].surname);
					});
			$( "#adminName" ).text(	data[0].mailAdmin);

			if(user === data[0].mailAdmin) //admin
			{
				$("#descriptionGroupStat").text(data[0].description);
				$("#descriptionGroupDyn").show();
				$("#changeDescrButton").show();
			}
			else
			{
				$("#descriptionGroupStat").text(data[0].description);
				$("#descriptionGroupDyn").hide();
				$("#changeDescrButton").hide();
			}
				});


		//___________________________________LIST MEMBERS______________________________________
		$.getJSON("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/members/" + group,
				function(data) 
				{
			$( "#list-users" ).replaceWith(	'<div id="list-users"> <a class="list-group-item">   <h4 class="list-group-item-heading">'
					+ data[0].name + " " + data[0].surname + " (" + data[0].mail + ")" + '</h4>	<h6 class="list-group-item-text">'
					+ data[0].biographie + '</h6> </a>  </div>');

			$("#nbMembers").text("| " + data.length);

			for(var i =1; i < data.length; i++)
			{
				$( "#list-users" ).append(	'<a class="list-group-item">   <h4 class="list-group-item-heading">'
						+ data[i].name + " " + data[i].surname + " (" + data[i].mail + ")" + '</h4>	<h6 class="list-group-item-text">'
						+ data[i].biographie + '</h6> </a>');
			}
				});

		//___________________________________DASHBOARD______________________________________
		var interval = setInterval(myTimer, 1000);
		function myTimer() 
		{
			$.getJSON("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/getMessages/" + group,
					function(data) 
					{
				$( "#dashboard" ).replaceWith(	'<span id="dashboard"><div class="list-group-item"><h4 class="list-group-item-heading">' + data[0].name + " " + data[0].surname + ' (<i>' + data[0].mail+ '</i>) wrote : </h4>' + 
						'<h6 class="list-group-item-text">' + data[0].Msg + '</h6></div></span>');
				for(var i =1; i < data.length; i++)
				{
					$( "#dashboard" ).append(	'<span id="dashboard"><div class="list-group-item"><h4 class="list-group-item-heading">' + data[i].name + " " + data[i].surname + '(<i>' + data[i].mail+ '</i>) wrote :</h4>' + 
							'<h6 class="list-group-item-text">' + data[i].Msg + '</h6></div></span>');
				}
					});
		}

		$(document).ready(function(){
			$("#sendMessageButton").click(function()
					{
				var infos = $("#sendMessage");
				if($("#messageZone").val() !== "")
				{
					$.post("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/writeMessage",
							infos.serialize(),
							function(data,status)
							{
						$("#messageZone").val("");
							});
				}
					});
		});
	}
})

//CHANGE DESCRIPTION
$(document).ready(function(){
	$("#changeDescrButton").click(function(){
		var infos = $("#infosDescr");
		$.ajax({
			url: "http://localhost:8080/MeetUp/MeetUpApp/MeetUp/changeGroupAttributes",
			type: "PUT", 
			data: infos.serialize(),
			success: function(data) 
			{
				document.location.href = "group.html";
			}
		});
	});
});
