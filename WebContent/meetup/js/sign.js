window.onload = function()
{	
	$("#signInSection").show();
	$('.signInSignUp').html('Sign In ! ');
	$("#signUpSection").hide();
};


$(document).ready(function(){
	$("#signInButton").click(function()
	{
		$.getJSON("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/viewProfile/" + $("#emailSignIn" ).val(),
				function(data) 
				{
					$.cookie("email", $("#emailSignIn" ).val()  , {expires:7, path:'/'});
					document.location.href = "home.html";
				}).error(function(data)
				{
					alert("Your address email is not registred. Please sign up before!");
			});
	});

		$('#toggle-event').change(function() {
			if($("#signInSection").is(":visible"))
			{
				$("#signInSection").hide();
				$("#signUpSection").show();
				$('.signInSignUp').html('Sign Up ! ');
			}
			else
			{
				$("#signInSection").show();
				$("#signUpSection").hide();
				$('.signInSignUp').html('Sign In ! ');
			}
		})
	
	$("#signUpButton").click(function()
	{
		if($("#name" ).val() != "" && $("#surname" ).val() != "" && $("#emailSignUp" ).val() != "" && $("#emailSignUp" ).val() === $("#emailSignUp2" ).val())
		{
			$.getJSON("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/viewProfile/" + $("#emailSignUp" ).val(),
			function(data) 
			{
				alert("User already exist. Please sign in.");
			}).error(function(data)
			{
				console.log("does not exist -> OK TO CREATE");
				var infos = $("#signUp");
				$.post("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/signUp",infos.serialize(),
				function(data) 
				{
					$.cookie("email", $("#emailSignUp" ).val()  , {expires:7, path:'/'});
					document.location.href = "home.html";
				});
			});
		}
	});
});