window.onload = function()
{	
	var user = $.cookie('email');
	var group = $.cookie('group');

	document.getElementById('email').value= user;
	document.getElementById('emailDelete').value= user;

	$.getJSON("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/viewProfile/" +user,
			function(data) {
		document.getElementById('name').value = data[0].name;
		document.getElementById('surname').value=data[0].surname;
		document.getElementById('bio').value=data[0].biographie;
	});

};

$(document).ready(function(){
	$("#changeButton").click(function(){
		if($("#name" ).val() != "" && $("#surname" ).val() != "" && $("#bio" ).val() != "")
		{
			var infos = $("#infos");
			$.ajax({
				url: "http://localhost:8080/MeetUp/MeetUpApp/MeetUp/changeUserAttributes",
				type: "PUT", 
				data: infos.serialize(),
				success: function(data) 
				{
					document.location.href = "home.html";
				}
			});
					
		}	
	});

	//POST BECAUSE WE HAVE TO UPDATE THE DATABASE BEFORE DELETING THE USER
	$("#deleteButton").click(function(){

		var infos = $("#infosDelete");
		$.post("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/deleteAccount",
				infos.serialize(),
				function(data) 
				{
					document.location.href = "signup.html";
				}).fail(data)
				{
					
				};
	});
});
