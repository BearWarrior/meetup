window.onload = function()
{	
	displayGroups();
};

function displayGroups()
{
	email = $.cookie('email');

	var noUser = (email === undefined);

	if(!noUser)
		document.getElementById( "createGroupEmail" ).value = email ;

	//________________________________________PROFILE USER_______________________________________
	if(!noUser)
	{
		$.getJSON("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/viewProfile/" +email,
				function(data) {
			document.getElementById('name_user').innerHTML =data[0].name;
			document.getElementById('lastname_user').innerHTML=data[0].surname;
			document.getElementById('email_user').innerHTML=data[0].mail;
			document.getElementById('bio').innerHTML=data[0].biographie;
		});


		//________________________________________MY GROUPS ADMIN_______________________________________
		$( "#myGroups" ).html('');
		$.getJSON("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/" +email +"/myGroups",
				function(data) {
			for(var i = 0; i < data.length; i++)
			{
				$( "#myGroups" ).append(	'<div id="mygroup1" class="col-sm-4 portfolio-item" >' + 
						'<h2><a class="groupLink" onClick="goToGroupPageWithCookie(\'' + data[i].nom + '\')">' + data[i].nom + ' </a></h2>' +
						'<p>' + data[i].description + '</p>' +
						'<p>' + "Owner: you (" + data[i].mailAdmin + ')</p>' +
						'<p>' + "<span>| " + data[i].nbMembers + "</span> <span class=\"glyphicon glyphicon-user\"></span>" +
						'<p><a class="groupLink" onClick="deleteGroup(\'' + data[i].nom + '\')">Close</a></p>' +
				'</div>');
			}
		});

		//________________________________________MY GROUPS MEMBERS_______________________________________
		$( "#groupsImIn" ).html('');
		$.getJSON("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/getGroupsOfUser/" +email,
				function(data) {
			for(var i = 0; i < data.length; i++)
			{
				$( "#groupsImIn" ).append(	'<div id="iparticip1" class="col-sm-4 portfolio-item">' + 
						'<h2><a class="groupLink"  onClick="goToGroupPageWithCookie(\'' + data[i].nom + '\')">' + data[i].nom + ' </a></h2>' +
						'<p>' + data[i].description + '</p>' +
						'<p>' + 'Owner: ' + data[i].mailAdmin + '</p>' +
						'<p>' + "<span>| " + data[i].nbMembers + "</span> <span class=\"glyphicon glyphicon-user\"></span>" +
						'<p><a class="groupLink" onClick="leaveGroup(\'' + data[i].nom + '\')">Leave</a></p>' +
				'</div> ');
			}
		});
		//________________________________________OTHER GROUPS_______________________________________
		$( "#otherGroups" ).html('');
		$.getJSON("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/getGroupsWhereUserNotMember/" +email,
				function(data) {
			for(var i = 0; i < data.length; i++)
			{
				$( "#otherGroups" ).append(	'<div id="other1" id="Group1" class="col-sm-4 portfolio-item">' + 
						'<h2>' + data[i].nom + '</h2>' +
						'<p>' + data[i].description + '</p>' +
						'<p>' + 'Owner: ' + data[i].mailAdmin + '</p>' +
						'<p>' + "<span>| " + data[i].nbMembers + "</span> <span class=\"glyphicon glyphicon-user\"></span>" +
						'<p><a class="groupLink" onClick="joinGroup(\'' + data[i].nom + '\')">Join</a></p>' +
				'</div> ');
			}
		});
	}
	else
	{
		document.getElementById('name_user').innerHTML = "<a href='signup.html'>Not connected (click to sign-up / sign-in)</a>";
		$( "#otherGroups" ).html('');
		$.getJSON("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/getGroupsWhereUserNotMember/" +email,
				function(data) {
			for(var i = 0; i < data.length; i++)
			{
				$( "#otherGroups" ).append(	'<div id="other1" id="Group1" class="col-sm-4 portfolio-item">' + 
						'<h2>' + data[i].nom + '</h2>' +
						'<p>' + data[i].description + '</p>' +
						'<p>' + 'Owner: ' + data[i].mailAdmin + '</p>' +
						'<p>' + "<span>| " + data[i].nbMembers + "</span> <span class=\"glyphicon glyphicon-user\"></span>" +
				'</div> ');
			}
		});
	}
}

function goToGroupPageWithCookie(nomG)
{
	$.cookie("group", nomG , {expires:7, path:'/'});
	document.location.href = "group.html";
}

function joinGroup(nomG)
{
	$.post("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/" + $.cookie("email") + "/joinGroup/" +nomG,
			function(data) {
	});
	displayGroups();
}

function leaveGroup(nomG)
{	
	$.ajax({
		url: "http://localhost:8080/MeetUp/MeetUpApp/MeetUp/" + $.cookie("email") + "/leaveGroup/" +nomG,
		type: "DELETE"
	});

	displayGroups();
}

function deleteGroup(nomG)
{	
	$.ajax({
		url: "http://localhost:8080/MeetUp/MeetUpApp/MeetUp/deleteGroup/" +nomG,
		type: "DELETE"
	});

	displayGroups();
}

$(document).ready(function(){
	$("#createGroupButton").click(function(){
	if($("#nomGroupeCreate" ).val() != "")
	{	
		$.cookie("group", $("#nomGroupeCreate" ).val()  , {expires:7, path:'/'});
		var infos = $("#createGroup");
		$.post("http://localhost:8080/MeetUp/MeetUpApp/MeetUp/createGroup",
				infos.serialize(),
				function(data,status){
			document.location.href = "group.html";
		}).error(function(data)
				{
			alert("A group with this name already exists!");
				});
			}
	else
	{
	alert("Enter a name for the group");
	}
	});
});


function changePage()
{
	if (email === undefined)
	{
		document.location.href="signup.html";
	}
	else
	{
		document.location.href="profile.html";
	}
}